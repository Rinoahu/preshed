preshed: Cython Hash Table for Pre-Hashed Keys
**********************************************

Simple but high performance Cython hash table mapping pre-randomized keys to void* values. Inspired by `Jeff Preshing <http://preshing.com/20130107/this-hash-table-is-faster-than-a-judy-array/>`_

.. image:: https://travis-ci.org/explosion/preshed.svg?branch=master
    :target: https://travis-ci.org/explosion/preshed
    :alt: Build Status

.. image:: https://img.shields.io/pypi/v/preshed.svg   
    :target: https://pypi.python.org/pypi/preshed
    :alt: pypi Version
